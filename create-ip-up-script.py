#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# Copyright (C) 2015 Xue Can <xuecan@gmail.com> and contributors.

import os
import pwd

FILENAME = "/etc/ppp/ip-up"
TEMPLATE = "#!/bin/sh\n\n%s/ensure-route-rules.py >> %s/debug.log 2>&1\n"

if __name__ == "__main__":
    path_to = os.path.dirname(__file__)
    if path_to == ".":
        path_to = os.getcwd()
    content = TEMPLATE % (path_to, path_to)
    with open(FILENAME, "w") as f:
        f.write(content)
    os.chmod(FILENAME, 0755)
