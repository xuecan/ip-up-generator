# ip-up 脚本生成器

## 目的

制作这个生成器主要出于两种用途：

1.  当使用 VPN 进行科学上网时，IP 地址通常位于境外，此时访问日常的许多娱乐服务，由于版权原因将被屏蔽。因此通过设置 `ip-up` 脚本仅在访问境外地址时才使用 VPN。
2.  工作中，许多生产环境都是封闭的内网，需要通过特定的 VPN 连入并配合适当的路由表才能工作，将这部分配置写在 `ip-up` 脚本中能够简化很多手工操作。


## 使用说明

### 依赖

*   一个或多个可用的 VPN，在该 VPN 的高级配置选项中，“通过 VPN 连接发送给所有流量”复选框**未被**勾选。
*   Python 2.x，使用 OS X 自带的即可。

### 编写配置文件

复制 `config.json.sample` 为 `config.json`，使用您喜欢的纯文本编辑器编辑 `config.json`。

配置信息是一个 JSON 对象，形如：

```json

{
  "special": [
    "VPN 0"
  ],
  "common": [
    {
      "name": "VPN 1",
      "for": ["192.168.20.0/25"]
    },
    {
      "name": "VPN 2",
      "for": ["10.0.0.0/24"],
      "but": ["10.0.0.1"]
    }
  ]
}
```

其中属性 `special` 是一个列表，列表的每个元素是一个 VPN 在 OS X 网络首选项中配置的名称。这里的 VPN 用于科学上网这个“特殊”目的。

属性 `common` 也是一个列表，列表的每个元素是一个用于表示 VPN 信息的对象，这里的 VPN 用于“常规”用途。这里，每个 VPN 信息是一个对象，必须具有名为 `name` 的属性，表示 VPN 在 OS X 网络首选项中配置的名称。可选包含属性 `for` 和 `but`，均为地址或网络列表。

`for` 用于表示该 VPN 适用的主机和/或网络。当需要指定主机时，使用形如 `10.0.0.1` 这样的 IP 地址，当需要指定网络时，使用形如 `10.0.0.0/24` 这样的形式，`/24` 表示子网掩码的二进制形式前面有 24 个 1，随后都是 0（即 `255.255.255.0`）。`but` 用于表示虽然在 `for` 范围内，但是仍然使用默认网关。

### 创建或更新中国大陆地区网络地址信息

执行：

```sh

./update-delegated-apnic.py
```

更新信息。这会生成 `delegated-to-cn.json` 文件。

### 通过命令行方式执行

每次连上一个 VPN 后，执行：

```sh

sudo ./ensure-route-rules.py
```

会完成必要的路由表配置。

### 通过 ip-up 触发

备份 `/etc/ppp/ip-up` 文件（如果存在的话），随后执行：

```sh

sudo ./create-ip-up-script.py
```

这会创建（或覆盖） `/etc/ppp/ip-up` 文件，在某个 VPN 连上时自动触发 `ensure-route-rules.py` 脚本。

此操作仅需进行一次。


## 背景知识

### 关于 ip-up

*   根据 [pppd 的 Man Page](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man8/pppd.8.html)，当一个 PPP 链接可以被用于发送和接收 IP 包时，将会触发 `/etc/ppp/ip-up` 脚本，并按顺序传入下列参数：`interface-name tty-device speed local-IP-address remote-IP-address ipparam`。
*   [这篇文章](http://www.macfreek.nl/memory/Modify_PPTP_Routing_Table) 提供了一个关于如何编写 `ip-up` 脚本的示例。
*   `ip-up` 在某些场景下，存在局限性。假设有两个可能同时需要使用的 VPN，我们说是连接机房甲和机房乙的，这两个 VPN 的网关配置都一样，假设是 192.168.10.1，当我们需要同时连接这两个 VPN，比如从机房甲内网读取一些数据，加工处理后写入机房乙内网的服务器，只使用 `ip-up` 的机制就无法分辨现在连上的是哪一个 VPN 了。因为，只有 `remote-IP-address` 这个参数用于辨别现在连上的 VPN，在这个例子中，两个 VPN 连上后得到的都是 192.168.10.1。
*   为了解决上面提出的问题，我们使用 `scutil --nc status "VPN配置名称"` 来检查特定的 VPN 是否已经连上。因此，`ip-up` 传入的参数不再重要，褪变成为一个纯粹的触发机制，让我们可以：
    *   使用 `netstat -nr` 检查当前的路由表；
    *   使用 `scutil --nc status "VPN配置名称"` 检查所有已连接的 VPN；
    *   根据当前路由表和配置，动态决定是否使用 `route` 命令添加或删除特定的路由规则。

### 关于中国大陆的所有 IP 地址

*   APNIC(亚太互联网络信息中心)所有 IP 地址的分配，可以通过其 FTP 服务获取，其中自然包含了所有分配给中国大陆地区使用的 IP 地址。详情请参考[ APNIC FTP 服务器上的 README 文档](ftp://ftp.apnic.net/public/stats/apnic/README.TXT)。

### 局限性

*   仅依靠 `ip-up` 被调用时得到的参数，如果存在多个 VPN 的网关地址是相同将无法区分，这个问题局限通过按 VPN 配置名称检查得以解决。
*   但是，如果多个 VPN 后面的私网 IP 地址或网络相同，即连上两个或更多 VPN 后，需要通过不同的 VPN 访问的目标地址，看起来是一样的，这种情况无法处理。
