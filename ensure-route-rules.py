#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# Copyright (C) 2015 Xue Can <xuecan@gmail.com> and contributors.

# 缺少下面三行将无法以 ip-up 形式运行，会出现编码出错的情况
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

"""
路由表更新工具
==============

用于 OS X 下，根据工作需要，连接 VPN 并配置路由表。
VPN 需要首先在网络偏好设置中设置好。
"""

# --------------------------------------------------------------
import os
import zlib
import base64
import pwd
from subprocess import call, check_output, CalledProcessError
from shlex import split
import json


# ============================================================ #
#                  ANSI Escape Code of Colors                  #
# ------------------------------------------------------------ #
exec(zlib.decompress(base64.b64decode("""     eNqdk0FvgjAYhu/8hV
2+i4GOhjh1FxMOiJ2SIS6IMWYHgtIZFgFDNWb/     fh+lirstJBzeNt/7PKEUd
+XFH04UsTAAG/Snz16a65rxtgxZPPEd952C     zCGbqjQLGQtU3jLfX27UYuKv
mYoLZ8aCyFErd+vcCpu5F+FQfKeuWERQ     XCXFgRvDPoVRn2jGBMU3vcxSL5P
Sy3zTq/FaL+NdL1eNXsZWr6h/9CPU     v9Z6uR87vo9zoTebRxSm3oJCsAwXjl
8XDBx9oTDAZ0A0d+Xh3jHJd2kC+zLlY3     AfzrUn97RjeWincgonW9cpCBtHW
yOhwO1SWMes4IKfxiB+hCXOaXk5W9cqO3Pj     ZOamMDnRUr67PBCf8zGgwtBB
t77LrDByZNXs9uCJKg3+0WpenJh3gvwcSMiKr7     JLv/4Mqt7Rv2aqP+zSV5c
CEeKy33MhukDk7UPENamKrDh0QTSXFhm8qsqqCwF/     BkJ+AYHICAo=""")))
# ------------------------------------------------------------ #
#     http://en.wikipedia.org/wiki/ANSI_escape_code#Colors     #
# ============================================================ #

# --------------------------------------------------------------
# 需要使用的命令
# --------------------------------------------------------------

# 查看路由表
SHOW_ROUTING_TABLES = "/usr/sbin/netstat -nr"
# 查看网络服务状态
SHOW_VPN_STATUS = "/usr/sbin/scutil --nc status \"%s\""
# 路由表操作
OPERATE_ROUTE_TABLE = "/sbin/route %s %s %s"

# --------------------------------------------------------------
# 主程序
# --------------------------------------------------------------

# 子网掩码清单，省去计算了
NETMASKS = {
    "0": "0.0.0.0",
    "1": "128.0.0.0",
    "2": "192.0.0.0",
    "3": "224.0.0.0",
    "4": "240.0.0.0",
    "5": "248.0.0.0",
    "6": "252.0.0.0",
    "7": "254.0.0.0",
    "8": "255.0.0.0",
    "9": "255.128.0.0",
    "10": "255.192.0.0",
    "11": "255.224.0.0",
    "12": "255.240.0.0",
    "13": "255.248.0.0",
    "14": "255.252.0.0",
    "15": "255.254.0.0",
    "16": "255.255.0.0",
    "17": "255.255.128.0",
    "18": "255.255.192.0",
    "19": "255.255.224.0",
    "20": "255.255.240.0",
    "21": "255.255.248.0",
    "22": "255.255.252.0",
    "23": "255.255.254.0",
    "24": "255.255.255.0",
    "25": "255.255.255.128",
    "26": "255.255.255.192",
    "27": "255.255.255.224",
    "28": "255.255.255.240",
    "29": "255.255.255.248",
    "30": "255.255.255.252",
    "31": "255.255.255.254",
    "32": "255.255.255.255"
}

class DATA:
    delegated = list()
    special_vpn_names = list()    # 特殊 VPN 名称清单
    common_vpn_names = list()     # 常规 VPN 名称清单
    common_vpn_settings = dict()  # 常规 VPN 配置信息
    known_route_rules = dict()    # 已生效的路由表
    default_gateway = None        # 默认网关
    default_interface = None      # 默认接口
    connected_vpns = list()       # 已连接的 VPN
    highlight_dests = list()      # 需要显示的路由表
    has_special_vpn_connected = False


class VPNDests(object):
    def __init__(self, name, for_, but_):
        self.name = name
        self.for_ = for_ if type(for_) is list else [for_]
        self.but_ = but_ if type(but_) is list else [but_]


class KnownRoute(object):
    def __init__(self, dest, gateway, interface):
        self.dest = dest
        self.gateway = gateway
        self.interface = interface


class ConnectedVPN(object):
    def __init__(self, name, gateway, interface):
        self.name = name
        self.gateway = gateway
        self.interface = interface


def load_config():
    info(u"(1/4) 正在加载配置文件...")
    file_location = os.path.dirname(__file__)
    filename = os.path.join(file_location, "config.json")
    with open(filename, "r") as f:
        config = json.load(f)
    # 常规 VPN
    commons = config.pop("common", list())
    for item in commons:
        name  = item["name"]
        DATA.common_vpn_names.append(name)
        for_ = item.get("for", list())
        but_ = item.get("but", list())
        DATA.common_vpn_settings[name] = VPNDests(name, for_, but_)
    success(u"    有 %d 个用于常规用途的 VPN" % len(DATA.common_vpn_names))
    # 特殊 VPN
    names = config.pop("special", list())
    if type(names) is list:
        DATA.special_vpn_names.extend(names)
    else:
        DATA.special_vpn_names.append(names)
    success(u"    有 %d 个用于特殊用途的 VPN" % len(DATA.special_vpn_names))
    # 如果需要加载 deligated-to-cn.json
    if DATA.special_vpn_names:
        filename = os.path.join(file_location, "delegated-to-cn.json")
        if os.path.exists(filename):
            with open(filename, "r") as f:
                DATA.delegated = json.load(f)
        if not DATA.delegated:
            warning(u"    缺少关于 CN 的网络信息，请先执行 ./update-delegated-apnic.py 程序")
        else:
            success(u"    有 %d 条关于 CN 的网络信息" % len(DATA.delegated))


def _get_net_dest(data):
    # netstat -nr 返回的网络形如 203.23.62/23
    # 这里还原为 203.23.62.0/23 的形式
    # 特别的， netstat -nr 对于 /24 直接省略 / 及其后的部分
    # 如 203.23.0 表示 203.23.0.0/24
    if ':' in data:
        # IPv6 不处理了
        return data
    # IPv4 的情况
    if '/' in data:
        n, t = data.split('/')
        t = "/" + t
    else:
        n = data
        t = "/24"
    dots = n.count(b'.')
    assert dots in [0, 1, 2, 3]
    n += b".0" * (3-dots)
    return n + t


def check_routing_tables():
    info(u"(2/4) 正在检查路由表...")
    result = check_output(split(SHOW_ROUTING_TABLES))
    for line in result.splitlines():
        if line.startswith(b"Internet6"):
            # 忽略关于 IPv6 的信息
            break
        words = line.split()
        #print(words)
        try:
            # output like:
            #     Destination        Gateway            Flags        Refs      Use   Netif Expire
            #     default            192.168.199.1      UGSc           13        0     en0
            dest = words[0]
            gateway = words[1]
            interface = words[5]
            assert dest != "Destination"
            assert not gateway.startswith("link#")
        except:
            continue
        if dest == b"default" and not gateway.startswith(b"link#"):
            DATA.default_gateway = gateway
            DATA.default_interface = interface
            success(u"    默认网关为: " + unicode(gateway) + u"；")
            continue
        if dest.count('.') != 3:
            # 形如 192.168.199.1/32 或 192.168/24 这样的形式
            dest = _get_net_dest(dest)
        DATA.known_route_rules[dest] = KnownRoute(dest, gateway, interface)
    success(u"    当前已配置了 %d 条路由规则。" % len(DATA.known_route_rules))
    if not DATA.default_gateway:
        print
        error(u"    没有发现默认网关，请检查网络是否可用。")
        sys.exit(1)


def _show_vpn_status(name):
    cmd = SHOW_VPN_STATUS % name.encode("utf8")
    try:
        result = check_output(split(cmd), stderr=open(os.devnull))
    except CalledProcessError as e:
        error("   ", name, u"未配置")
        return
    if result.startswith("Disconnected"):
        warning("   ", name, u"断开")
        return
    if result.startswith("Connecting"):
        warning("   ", name, u"连接中")
        return
    if result.startswith("Connected"):
        interface = None
        gateway = None
        for line in result.splitlines():
            words = line.split()
            if words and words[0] == "InterfaceName":
                interface = words[2]  # words[1]=':'
            if words and words[0] == "Router":
                gateway = words[2]  # words[1]=':'
        if interface and gateway:
            success("   ", name, u"已连接(%s)" % interface)
            return ConnectedVPN(name, gateway, interface)
        else:
            raise RuntimeError("cannot get vpn's interface or route")
    raise RuntimeError("unknown status '%s' for %s" % (result, name))


def check_vpn_connections():
    info(u"(3/4) 正在检查已连接的 VPN...")
    for name in DATA.common_vpn_names:
        record = _show_vpn_status(name)
        if record:
            DATA.connected_vpns.append(record)
    for name in DATA.special_vpn_names:
        record = _show_vpn_status(name)
        if record:
            DATA.connected_vpns.append(record)
            DATA.has_special_vpn_connected = True
            break  # 特殊 VPN 检查到一个就不检查了
    if not DATA.connected_vpns:
        print
        error(u"已知的 VPN 均未连接，请连接上 VPN 后再次执行本程序。")
        #sys.exit(2)


def _route_operate(action, dest, gateway="", interface=""):
    if action.startswith('a'):
        action = "add"
    elif action.startswith('d'):
        action = "delete"
    if gateway and interface:
        raise ValueError("either gateway, or interface, not both")
    if not gateway and not interface:
        raise ValueError("either gateway, or interface, not none")
    if '/' in dest:
        network, padding = dest.split('/')
        netmask = NETMASKS[padding]
        dest = ' '.join(["-net", network, "-netmask", netmask])
    else:
        dest = ' '.join(["-host", dest])
    if gateway:
        via = gateway
    else:
        via = "-interface " + interface
    cmd = OPERATE_ROUTE_TABLE % (action, dest, via)
    check_output(split(cmd))


def _set_route_rule(dest, via):
    DATA.highlight_dests.append(dest)
    if dest in DATA.known_route_rules:
        known = DATA.known_route_rules[dest]
        if via=="default":
            if known.gateway==DATA.default_gateway:
                return
            else:
                _route_operate("del", dest, known.gateway)
        else:
            if known.interface==via.interface:
                return
            else:
                _route_operate("del", dest, interface=known.interface)
    if via=="default":
        _route_operate("add", dest, DATA.default_gateway)
    else:
        _route_operate("add", dest, interface=via.interface)


def _unset_route_rule(dest):
    if dest in DATA.known_route_rules:
        known = DATA.known_route_rules[dest]
        _route_operate("del", dest, known.gateway)


def apply_vpn_dest_gatways():
    info(u"(4/4) 正在为已连接的 VPN 创建路由规则...")
    for record in DATA.connected_vpns:
        name = record.name
        interface = record.interface
        if name not in DATA.special_vpn_names:
            # 非特殊 VPN，按配置执行操作
            conf = DATA.common_vpn_settings[name]
            for dest in conf.but_:
                _set_route_rule(dest, "default")
            for dest in conf.for_:
                _set_route_rule(dest, record)
        else: # name in DATA.special_vpn_names
            for dest in DATA.delegated:
                _set_route_rule(dest, "default")
            _set_route_rule(u"0.0.0.0/0", record)
    success(u"操作完成。")


def show_route_table():
    info(u"当前路由表信息如下：")
    print
    output = check_output(split(SHOW_ROUTING_TABLES))
    for line in output.splitlines():
        if line.startswith("Internet6"):
            break
        words = line.split()
        try:
            dest = words[0]
            if dest.count(".")!=3:
                dest = _get_net_dest(dest)
            gateway = words[1]
            interface = words[5]
        except:
            continue
        if dest=="Destination":
            info(line)
            continue
        if dest=="default" and "link" not in gateway:
            success(line)
            continue
        if dest in DATA.highlight_dests:
            print(line)
        continue
    #info(u"==================== ====================")


def main():
    if pwd.getpwuid(os.getuid()).pw_name != "root":
        raise RuntimeError("supervisor required")
    load_config()
    check_routing_tables()
    check_vpn_connections()
    apply_vpn_dest_gatways()
    show_route_table()

if __name__ == "__main__":
    main()


# TODO: 运行时间较长，应该在 /var/run 写一个锁避免重复执行
# TODO: 清理使用默认路由的那些规则
