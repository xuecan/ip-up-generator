#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# Copyright (C) 2015 Xue Can <xuecan@gmail.com> and contributors.

import os
import ftplib
import math
import json

APNIC_SERVER = "ftp.apnic.net"
APNIC_PATH = "/public/stats/apnic/"
APNIC_FILENAME = "delegated-apnic-latest"
FILENAME = "delegated-to-cn.json"

def _fetch_lines():
    """从 APNIC 服务器下载网络分配信息"""
    ftp = ftplib.FTP(APNIC_SERVER)
    ftp.login()
    ftp.cwd(APNIC_PATH)
    lines = list()
    ftp.retrlines("RETR " + APNIC_FILENAME, lines.append)
    ftp.quit()
    return lines


def _parse_lines(lines):
    """返回中国大陆地区的网络地址列表，列表的每个条目是形如 `202.173.0.0/22` 的字符串。"""
    networks = list()
    for line in lines:
        if line.startswith("apnic|CN|ipv4|"):
            data = line.split("|")
            # data 形如 ['apnic', 'CN', 'ipv4', '202.173.0.0', '1024', '20110414', 'allocated']
            network_addr = data[3]
            addrs_count = int(data[4])
            padding = 32 - int(math.log(addrs_count, 2))  # 1024 相当于 22，256 相当于 24(正好是一个C)
            networks.append("%s/%d" % (network_addr, padding))
    return networks


def _save_networks(networks):
    filename = os.path.join(os.path.dirname(__file__), FILENAME)
    with open(filename, "w") as f:
        json.dump(networks, f)


if __name__ == "__main__":
    lines = _fetch_lines()
    networks = _parse_lines(lines)
    _save_networks(networks)
